import React from "react";
import { render } from "react-dom";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

function makeData(len = 20) {
    return [
        {skill: "Talking", value: 5, type: "Social",},
        {skill: "Persuasion", value: 3, type: "Social",},
        {skill: "Gun", value: 2, type: "Combat",},
        {skill: "Melee", value: 4, type: "Combat",},
        {skill: "Electronics", value: 1, type: "Technical",},
    ]
}

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            data: makeData()
        };
    }
    render() {
        const { data } = this.state;
        return (
            <div>
                <ReactTable
                    data={data}
                    columns={[
                        {
                            Header: "Skill",
                            accessor: "skill"
                        },
                        {
                            Header: "Value",
                            accessor: "value"
                        },
                        {
                            Header: 'Type',
                            accessor: "type"
                        }
                    ]}
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
            </div>
        );
    }
}

render(<App />, document.getElementById("root"));
